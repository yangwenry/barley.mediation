package barley.mediation.rest.api;

import org.apache.axis2.engine.AxisConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ConfigHolder {
	
	private static ConfigHolder instance;
    private static final Log log = LogFactory.getLog(ConfigHolder.class);

	private AxisConfiguration axisConfiguration;
	
	private ConfigHolder() {
    }

    public static ConfigHolder getInstance() {
        if (instance == null) {
            instance = new ConfigHolder();
        }
        return instance;
    }

	public AxisConfiguration getAxisConfiguration() {
		return axisConfiguration;
	}

	public void setAxisConfiguration(AxisConfiguration axisConfiguration) {
		this.axisConfiguration = axisConfiguration;
	}
	
	
}

